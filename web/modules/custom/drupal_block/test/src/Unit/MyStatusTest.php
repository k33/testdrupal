<?php

namespace Drupal\Tests\drupal_block\Unit;

use Drupal\drupal_block\MyStatus;
use Drupal\Tests\UnitTestCase;

/**
 * Unit tests for the MyStatus utility class.
 *
 * @group drupal_block
 */
class MyStatusTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    // Nothing to do here.
    parent::setUp();
  }

  /**
   * Tests the constant to numeric method.
   */
  public function testConstantToNumeric() {
    $this->assertEquals(MyStatus::UNKNOWN, MyStatus::constantToNumeric('my_unknown_status'));

    $this->assertEquals(MyStatus::GREEN, MyStatus::constantToNumeric('my_green_status'));

    $this->assertEquals(MyStatus::YELLOW, MyStatus::constantToNumeric('my_yellow_status'));

    $this->assertEquals(MyStatus::RED, MyStatus::constantToNumeric('my_red_status'));

    $this->assertEquals(MyStatus::CRITICAL, MyStatus::constantToNumeric('my_critical_status'));

  }

  /**
   * Tests the numeric to constant method.
   */
  public function testNumericToConstant() {
    $this->assertEquals(MyStatus::numericToConstant(MyStatus::UNKNOWN), 'my_unknown_status');

    $this->assertEquals(MyStatus::numericToConstant(MyStatus::GREEN), 'my_green_status');

    $this->assertEquals(MyStatus::numericToConstant(MyStatus::YELLOW), 'my_yellow_status');

    $this->assertEquals(MyStatus::numericToConstant(MyStatus::RED), 'my_red_status');

    $this->assertEquals(MyStatus::numericToConstant(MyStatus::CRITICAL), 'my_critical_status');

    $this->assertFalse(MyStatus::numericToConstant(-9999));
  }



}
