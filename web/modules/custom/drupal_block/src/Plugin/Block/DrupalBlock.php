<?php

namespace Drupal\drupal_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;

use Drupal\Core\Render\Markup;

/**
 * Provide Drupal block.
 *
 * @Block(
 *   id = "drupal_block",
 *   admin_label = @Translation("Drupal block"),
 * )
 */
class DrupalBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#description' => $this->t('Title in right column.'),
      '#default_value' => isset($config['title']) ? $config['title']: '',
    ];

    $form['text'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('text'),
      '#description' => $this->t('HTML to be displayed.'),
      '#default_value' => isset($config['text']) ? $config['text']['value'] : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['text'] = $values['text'];
    $this->configuration['title'] = $values['title'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $config = $this->configuration;

    $build['drupal_block'] = [
      '#theme' => 'drupal_block',
      '#content' => [
        'title' => isset($config['title']) ? $config['title'] : '',
        'text' => Markup::create(isset($config['text']) ? $config['text']['value'] : ''),
      ]
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), [
      'languages:language_interface',
    ]);
  }

}
